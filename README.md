# HTMLjs
![Build Status](https://badgen.net/bundlephobia/min/@ayn/htmljs) ![Build Status](https://badgen.net/bundlephobia/minzip/@ayn/htmljs)

A tiny beautiful HTML markup generator.

Core for [PageJS](https://www.npmjs.com/package/@ayn/pagejs).
Need XML? Checkout [XMLjs](https://www.npmjs.com/package/@ayn/xmljs).

## API
```js
const htmljs = require('@ayn/htmljs')

let page = new htmljs({ title: 'Hello World!' })
    .div(el=>{
           el.span('I am a span inside a div.')
           el.h1({ style: 'color: red;' },'Cool, uh?', el=>{
                el.br()
                el.span('I am inside h1, so I am red too!')
           })
    })

let markup = page.getMarkup()
```

## Output
```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Hello World!</title>
    </head>
    <body>
        <div><span>I am a span inside a div.</span>
            <h1 style="color: red;">Cool, uh?<br><span>I am inside h1, so I am red too!</span></h1>
        </div>
    </body>
</html>

```

### Constructor
Constructor takes a single object used for configuration, here are the default values:
```js
{
    doctype: '<!doctype html>',
    lang: 'en',
    charset: 'utf-8',
    title: 'Untitled paged',
    body: 'No content.',
    theme: null,
    tags: {
        self_closing: [
            'area', 'base', 'br', 'col', 'embed', 'wbr', 'input',
            'img', 'source', 'link', 'meta', 'param', 'hr', 'track'
        ],
        autoclose: false,
    }
}
```

### Methods
There is only one method. 
```js
page.getMarkup()
```
Returns the markup code of the page or in the case of a child element (obtained through a tag callback) a subset of it.

Child elements also contain two properties `el.parent` and `el.siblings`.

You can also process objects directly. 
```js
new htmljs().getMarkup({
    div: {
        attributes: { style: 'background: purple' },
        content: 'wowza',
        children: []
    }
})
```

## License
  [MIT](LICENSE)