class HTMLjs {
    constructor(config = {}) {
        // Default values
        this.config = Object.assign({
            doctype: '<!doctype html>',
            lang: 'en',
            charset: 'utf-8',
            title: 'Untitled paged',
            body: 'No content.',
            theme: null,
            tags: {
                self_closing: [
                    'area', 'base', 'br', 'col', 'embed', 'wbr', 'input',
                    'img', 'source', 'link', 'meta', 'param', 'hr', 'track'
                ],
                autoclose: false,
            }
        }, config)
        
        // Basic html structure
        this.dom={
            html:{attributes:{lang:this.config.lang},children:[
                {head:{children:[
                    {meta:{attributes:{charset:this.config.charset}}},
                    {title:{content:this.config.title}}
                ]}},
                {body:{children:[]}}
        ]}}

        // Allow wild card functions
        return new Proxy(this, this)
    }

    get(target, prop) {

        // If it exists return appropriate method
        if(this[prop]) return this[prop]

        // Run our function
        return function(...Args) {

            // Work around node utils
            if(typeof prop == 'symbol') return this

            // defaults
            let attributes = {},
                content = '',
                cb = ()=>{},
                target = (Args[3]) ? Args[3].target : this.dom.html.children[1].body.children,
                children = [],
                parent = (Args[3]) ? Args[3].parent : null
                
            // Adjust first 3 args by type
            for(let [i, arg] of Args.entries()) {
                if(i == 3 ) break                   
                switch(typeof arg) {
                    case 'object':
                        attributes = arg
                        break;
                    case 'string':
                        content = arg
                        break;
                    case 'function':
                        cb = arg
                        break;
                    } 
            }
    
            // New branch
            let obj = {
                [prop]: {
                    attributes,
                    content,
                    children,
                }
            }

            let cb_parent = {
                parent,
                target: obj[prop].children
            }

            // Append to parent
            if(target.push) target.push(obj)
                else target[prop] = obj

            // Modify cb to append to right object
            cb(new Proxy({}, {
                get:(target, sub_prop)=>{
                    // Prop seems to be symbol when loggin
                    if(typeof sub_prop != 'symbol') {
                        // Preset
                        if(sub_prop=='getMarkup') return ()=>this.getMarkup(obj)
                        else if(sub_prop=='parent') return prop
                        else if(sub_prop=='siblings') return obj[prop].children
                        
                        // Catch all (element)
                        else return (attributes, sub_content, cb)=> 
                            this[sub_prop](attributes, sub_content, cb, cb_parent)
                    }
                    // Perhaps append content to a value so user can place it himself
                    else return () => { return { parent: prop, siblings: obj[prop].children } } 
                }
            }))

            return this

        }   
    }

    getMarkup(obj) {

        if(!obj) obj=this.dom
        
        if(obj.html) var markup = this.config.doctype
            else var markup = ''

        let parse = (obj, content ='') => {

            let markup = ''

            for(let el in obj) {

                let formatted_attributes = ''
                
                // Handle attributes
                if(obj[el].attributes)
                    for(let attribute in obj[el].attributes) 
                        formatted_attributes += `${attribute}='${obj[el].attributes[attribute]}'`
                
                // Handle content
                if(typeof obj[el].content == 'string')   
                    content = obj[el].content

                let inner_markup = ''

                // Handle children
                if(obj[el].children) 
                    if(obj[el].children.forEach) {
                        obj[el].children.forEach(child=>{
                            inner_markup += parse(child, content)
                        })
                    }
                
                // Generate tag
                markup += `<${el}${(formatted_attributes) ? ' '+formatted_attributes:""}${(this.config.tags.autoclose)?" /":""}>${(!this.config.tags.self_closing.includes(el))? content+inner_markup+'</'+el+'>' : ''}`

                return markup
            }

        }

        markup += parse(obj || this.dom)

        return markup
        
    }
}

module.exports = HTMLjs